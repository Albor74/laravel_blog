<?php

namespace App\Http\Controllers\Admin\Tag;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tag\StoreRequest;
use App\Models\Tag;
use GuzzleHttp\Psr7\Request;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request ->validated();
        Tag::firstOrCreate($data);
        return redirect()->route('tag.index');

    }
}
