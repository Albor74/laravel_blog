@extends('admin.layouts.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Добавление поста</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard v1</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div>
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">

                        <form  action="{{ route('post.store') }}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-group w-25">
                                <input type="text" class="form-control" name="title" placeholder="Название поста"
                                value="{{ old('title') }}">
                                @error('title')
                                <div class="text-danger">
                                    Это поле необходимо заполнить
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <textarea id="summernote" name="content">
                                    {{ old('content') }}
                                </textarea>
                            </div>
                            @error('content')
                            <div class="text-danger">
                                Это поле необходимо заполнить
                            </div>
                            @enderror
                            <div class="form-group w-50">
                                <label>Добавить главное изображение</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="main_image" >
                                        <label class="custom-file-label">Выберите изображение</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Загрузить</span>
                                    </div>
                                </div>
                            </div>
                            @error('main_image')
                            <div class="text-danger">
                                Это поле необходимо заполнить
                            </div>
                            @enderror
                            <div class="form-group w-50">
                                <label>Добавить превью</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="preview_image" >
                                        <label class="custom-file-label">Выберите изображение</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Загрузить</span>
                                    </div>
                                </div>
                            </div>
                            @error('preview_image')
                            <div class="text-danger">
                                Это поле необходимо заполнить
                            </div>
                            @enderror

                            <div class="form-group w-50">
                                <label>Выберете категорию</label>
                                <select class="form-control" name="category_id">
                                    @foreach($categories as $category)
                                    <option value="{{ $category->id }}" {{ $category->id == old('category_id') ? 'selected' : '' }}>
                                        {{ $category->title }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group w-50">
                                <label>Выберете тэги</label>
                                <select class="select2 " name="tag_ids[]" multiple="multiple" data-placeholder="Тэги" style="width: 100%;" data-select2-id="7" tabindex="-1" aria-hidden="true">
                                    @foreach($tags as $tag)
                                        <option {{ is_array( old('tag_ids')) && in_array($tag->id, old('tag_ids')) ? 'selected': '' }} value="{{ $tag->id }}">{{ $tag->title }}</option>
                                    @endforeach
                                </select>
                            </div>

                           <div class="form-group">
                               <input type="submit" class="btn btn-primary" value="Сохранить">
                           </div>
                        </form>
                    </div>

                </div>

            </div>
        </section>

    </div>
@endsection
